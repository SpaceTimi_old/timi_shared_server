-- MySQL dump 10.13  Distrib 8.0.11, for macos10.13 (x86_64)
--
-- Host: localhost    Database: std
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `meta_appconstant`
--

DROP TABLE IF EXISTS `meta_appconstant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `meta_appconstant` (
  `id` varchar(50) NOT NULL DEFAULT '',
  `value` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_appconstant`
--

LOCK TABLES `meta_appconstant` WRITE;
/*!40000 ALTER TABLE `meta_appconstant` DISABLE KEYS */;
INSERT INTO `meta_appconstant` VALUES ('defaultCountryId','US'),('test','42');
/*!40000 ALTER TABLE `meta_appconstant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_country`
--

DROP TABLE IF EXISTS `meta_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `meta_country` (
  `id` varchar(10) NOT NULL,
  `common_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  `latitude` float NOT NULL,
  `longitude` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_country`
--

LOCK TABLES `meta_country` WRITE;
/*!40000 ALTER TABLE `meta_country` DISABLE KEYS */;
INSERT INTO `meta_country` VALUES ('AD','Andorra',42.5462,1.60155),('AE','United Arab Emirates',23.4241,53.8478),('AF','Afghanistan',33.9391,67.71),('AG','Antigua and Barbuda',17.0608,-61.7964),('AI','Anguilla',18.2206,-63.0686),('AL','Albania',41.1533,20.1683),('AM','Armenia',40.0691,45.0382),('AN','Netherlands Antilles',12.2261,-69.0601),('AO','Angola',-11.2027,17.8739),('AQ','Antarctica',-75.251,-0.071389),('AR','Argentina',-38.4161,-63.6167),('AS','American Samoa',-14.271,-170.132),('AT','Austria',47.5162,14.5501),('AU','Australia',-25.2744,133.775),('AW','Aruba',12.5211,-69.9683),('AZ','Azerbaijan',40.1431,47.5769),('BA','Bosnia and Herzegovina',43.9159,17.6791),('BB','Barbados',13.1939,-59.5432),('BD','Bangladesh',23.685,90.3563),('BE','Belgium',50.5039,4.46994),('BF','Burkina Faso',12.2383,-1.56159),('BG','Bulgaria',42.7339,25.4858),('BH','Bahrain',25.9304,50.6378),('BI','Burundi',-3.37306,29.9189),('BJ','Benin',9.30769,2.31583),('BM','Bermuda',32.3214,-64.7574),('BN','Brunei',4.53528,114.728),('BO','Bolivia',-16.2902,-63.5887),('BR','Brazil',-14.235,-51.9253),('BS','Bahamas',25.0343,-77.3963),('BT','Bhutan',27.5142,90.4336),('BV','Bouvet Island',-54.4232,3.41319),('BW','Botswana',-22.3285,24.6849),('BY','Belarus',53.7098,27.9534),('BZ','Belize',17.1899,-88.4977),('CA','Canada',56.1304,-106.347),('CC','Cocos [Keeling] Islands',-12.1642,96.871),('CD','Congo [DRC]',-4.03833,21.7587),('CF','Central African Republic',6.61111,20.9394),('CG','Congo [Republic]',-0.228021,15.8277),('CH','Switzerland',46.8182,8.22751),('CI','Cote d\' Ivoire',7.53999,-5.54708),('CK','Cook Islands',-21.2367,-159.778),('CL','Chile',-35.6751,-71.543),('CM','Cameroon',7.36972,12.3547),('CN','China',35.8617,104.195),('CO','Colombia',4.57087,-74.2973),('CR','Costa Rica',9.74892,-83.7534),('CU','Cuba',21.5218,-77.7812),('CV','Cape Verde',16.0021,-24.0132),('CX','Christmas Island',-10.4475,105.69),('CY','Cyprus',35.1264,33.4299),('CZ','Czech Republic',49.8175,15.473),('DE','Germany',51.1657,10.4515),('DJ','Djibouti',11.8251,42.5903),('DK','Denmark',56.2639,9.50179),('DM','Dominica',15.415,-61.371),('DO','Dominican Republic',18.7357,-70.1627),('DZ','Algeria',28.0339,1.65963),('EC','Ecuador',-1.83124,-78.1834),('EE','Estonia',58.5953,25.0136),('EG','Egypt',26.8206,30.8025),('EH','Western Sahara',24.2155,-12.8858),('ER','Eritrea',15.1794,39.7823),('ES','Spain',40.4637,-3.74922),('ET','Ethiopia',9.145,40.4897),('FI','Finland',61.9241,25.7482),('FJ','Fiji',-16.5782,179.414),('FK','Falkland Islands [Islas Malvinas]',-51.7963,-59.5236),('FM','Micronesia',7.42555,150.551),('FO','Faroe Islands',61.8926,-6.91181),('FR','France',46.2276,2.21375),('GA','Gabon',-0.803689,11.6094),('GB','United Kingdom',55.3781,-3.43597),('GD','Grenada',12.2628,-61.6042),('GE','Georgia',42.3154,43.3569),('GF','French Guiana',3.93389,-53.1258),('GG','Guernsey',49.4657,-2.58528),('GH','Ghana',7.94653,-1.02319),('GI','Gibraltar',36.1377,-5.34537),('GL','Greenland',71.7069,-42.6043),('GM','Gambia',13.4432,-15.3101),('GN','Guinea',9.94559,-9.69664),('GP','Guadeloupe',16.996,-62.0676),('GQ','Equatorial Guinea',1.6508,10.2679),('GR','Greece',39.0742,21.8243),('GS','South Georgia and the South Sandwich Islands',-54.4296,-36.5879),('GT','Guatemala',15.7835,-90.2308),('GU','Guam',13.4443,144.794),('GW','Guinea-Bissau',11.8037,-15.1804),('GY','Guyana',4.86042,-58.9302),('GZ','Gaza Strip',31.3547,34.3088),('HK','Hong Kong',22.3964,114.109),('HM','Heard Island and McDonald Islands',-53.0818,73.5042),('HN','Honduras',15.2,-86.2419),('HR','Croatia',45.1,15.2),('HT','Haiti',18.9712,-72.2852),('HU','Hungary',47.1625,19.5033),('ID','Indonesia',-0.789275,113.921),('IE','Ireland',53.4129,-8.24389),('IL','Israel',31.0461,34.8516),('IM','Isle of Man',54.2361,-4.54806),('IN','India',20.5937,78.9629),('IO','British Indian Ocean Territory',-6.34319,71.8765),('IQ','Iraq',33.2232,43.6793),('IR','Iran',32.4279,53.688),('IS','Iceland',64.9631,-19.0208),('IT','Italy',41.8719,12.5674),('JE','Jersey',49.2144,-2.13125),('JM','Jamaica',18.1096,-77.2975),('JO','Jordan',30.5852,36.2384),('JP','Japan',36.2048,138.253),('KE','Kenya',-0.023559,37.9062),('KG','Kyrgyzstan',41.2044,74.7661),('KH','Cambodia',12.5657,104.991),('KI','Kiribati',-3.37042,-168.734),('KM','Comoros',-11.875,43.8722),('KN','Saint Kitts and Nevis',17.3578,-62.783),('KP','North Korea',40.3399,127.51),('KR','South Korea',35.9078,127.767),('KW','Kuwait',29.3117,47.4818),('KY','Cayman Islands',19.5135,-80.567),('KZ','Kazakhstan',48.0196,66.9237),('LA','Laos',19.8563,102.495),('LB','Lebanon',33.8547,35.8623),('LC','Saint Lucia',13.9094,-60.9789),('LI','Liechtenstein',47.166,9.55537),('LK','Sri Lanka',7.87305,80.7718),('LR','Liberia',6.42805,-9.4295),('LS','Lesotho',-29.61,28.2336),('LT','Lithuania',55.1694,23.8813),('LU','Luxembourg',49.8153,6.12958),('LV','Latvia',56.8796,24.6032),('LY','Libya',26.3351,17.2283),('MA','Morocco',31.7917,-7.09262),('MC','Monaco',43.7503,7.41284),('MD','Moldova',47.4116,28.3699),('ME','Montenegro',42.7087,19.3744),('MG','Madagascar',-18.7669,46.8691),('MH','Marshall Islands',7.13147,171.184),('MK','Macedonia [FYROM]',41.6086,21.7453),('ML','Mali',17.5707,-3.99617),('MM','Myanmar [Burma]',21.914,95.9562),('MN','Mongolia',46.8625,103.847),('MO','Macau',22.1987,113.544),('MP','Northern Mariana Islands',17.3308,145.385),('MQ','Martinique',14.6415,-61.0242),('MR','Mauritania',21.0079,-10.9408),('MS','Montserrat',16.7425,-62.1874),('MT','Malta',35.9375,14.3754),('MU','Mauritius',-20.3484,57.5522),('MV','Maldives',3.20278,73.2207),('MW','Malawi',-13.2543,34.3015),('MX','Mexico',23.6345,-102.553),('MY','Malaysia',4.21048,101.976),('MZ','Mozambique',-18.6657,35.5296),('NA','Namibia',-22.9576,18.4904),('NC','New Caledonia',-20.9043,165.618),('NE','Niger',17.6078,8.08167),('NF','Norfolk Island',-29.0408,167.955),('NG','Nigeria',9.082,8.67528),('NI','Nicaragua',12.8654,-85.2072),('NL','Netherlands',52.1326,5.29127),('NO','Norway',60.472,8.46895),('NP','Nepal',28.3949,84.124),('NR','Nauru',-0.522778,166.932),('NU','Niue',-19.0544,-169.867),('NZ','New Zealand',-40.9006,174.886),('OM','Oman',21.5126,55.9233),('PA','Panama',8.53798,-80.7821),('PE','Peru',-9.18997,-75.0152),('PF','French Polynesia',-17.6797,-149.407),('PG','Papua New Guinea',-6.31499,143.956),('PH','Philippines',12.8797,121.774),('PK','Pakistan',30.3753,69.3451),('PL','Poland',51.9194,19.1451),('PM','Saint Pierre and Miquelon',46.9419,-56.2711),('PN','Pitcairn Islands',-24.7036,-127.439),('PR','Puerto Rico',18.2208,-66.5901),('PS','Palestinian Territories',31.9522,35.2332),('PT','Portugal',39.3999,-8.22445),('PW','Palau',7.51498,134.583),('PY','Paraguay',-23.4425,-58.4438),('QA','Qatar',25.3548,51.1839),('RE','Reunion',-21.1151,55.5364),('RO','Romania',45.9432,24.9668),('RS','Serbia',44.0165,21.0059),('RU','Russia',61.524,105.319),('RW','Rwanda',-1.94028,29.8739),('SA','Saudi Arabia',23.8859,45.0792),('SB','Solomon Islands',-9.64571,160.156),('SC','Seychelles',-4.67957,55.492),('SD','Sudan',12.8628,30.2176),('SE','Sweden',60.1282,18.6435),('SG','Singapore',1.35208,103.82),('SH','Saint Helena',-24.1435,-10.0307),('SI','Slovenia',46.1512,14.9955),('SJ','Svalbard and Jan Mayen',77.5536,23.6703),('SK','Slovakia',48.669,19.699),('SL','Sierra Leone',8.46056,-11.7799),('SM','San Marino',43.9424,12.4578),('SN','Senegal',14.4974,-14.4524),('SO','Somalia',5.15215,46.1996),('SR','Suriname',3.91931,-56.0278),('ST','Sao Tome and Principe',0.18636,6.61308),('SV','El Salvador',13.7942,-88.8965),('SY','Syria',34.8021,38.9968),('SZ','Swaziland',-26.5225,31.4659),('TC','Turks and Caicos Islands',21.694,-71.7979),('TD','Chad',15.4542,18.7322),('TF','French Southern Territories',-49.2804,69.3486),('TG','Togo',8.61954,0.824782),('TH','Thailand',15.87,100.993),('TJ','Tajikistan',38.861,71.2761),('TK','Tokelau',-8.96736,-171.856),('TL','Timor-Leste',-8.87422,125.728),('TM','Turkmenistan',38.9697,59.5563),('TN','Tunisia',33.8869,9.5375),('TO','Tonga',-21.179,-175.198),('TR','Turkey',38.9637,35.2433),('TT','Trinidad and Tobago',10.6918,-61.2225),('TV','Tuvalu',-7.10954,177.649),('TW','Taiwan',23.6978,120.961),('TZ','Tanzania',-6.36903,34.8888),('UA','Ukraine',48.3794,31.1656),('UG','Uganda',1.37333,32.2903),('US','United States',37.0902,-95.7129),('UY','Uruguay',-32.5228,-55.7658),('UZ','Uzbekistan',41.3775,64.5853),('VA','Vatican City',41.9029,12.4534),('VC','Saint Vincent and the Grenadines',12.9843,-61.2872),('VE','Venezuela',6.42375,-66.5897),('VG','British Virgin Islands',18.4207,-64.64),('VI','U.S. Virgin Islands',18.3358,-64.8963),('VN','Vietnam',14.0583,108.277),('VU','Vanuatu',-15.3767,166.959),('WF','Wallis and Futuna',-13.7688,-177.156),('WS','Samoa',-13.759,-172.105),('XK','Kosovo',42.6026,20.903),('YE','Yemen',15.5527,48.5164),('YT','Mayotte',-12.8275,45.1662),('ZA','South Africa',-30.5595,22.9375),('ZM','Zambia',-13.1339,27.8493),('ZW','Zimbabwe',-19.0154,29.1549);
/*!40000 ALTER TABLE `meta_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_time` datetime DEFAULT NULL,
  `last_login_time` datetime DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `USER_ID_UNIQUE` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (4,'2018-10-13 01:19:39','2018-10-13 02:35:55');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_country`
--

DROP TABLE IF EXISTS `user_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_country` (
  `user_id` bigint(20) NOT NULL,
  `country_id` varchar(20) NOT NULL DEFAULT '',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `USER_ID_UNIQUE` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_country`
--

LOCK TABLES `user_country` WRITE;
/*!40000 ALTER TABLE `user_country` DISABLE KEYS */;
INSERT INTO `user_country` VALUES (4,'US');
/*!40000 ALTER TABLE `user_country` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-13  3:49:40
