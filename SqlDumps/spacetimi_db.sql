-- MySQL dump 10.13  Distrib 8.0.11, for macos10.13 (x86_64)
--
-- Host: localhost    Database: spacetimi
-- ------------------------------------------------------
-- Server version	8.0.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `account` (
  `account_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_time` datetime DEFAULT NULL,
  PRIMARY KEY (`account_id`),
  UNIQUE KEY `ACCOUNT_ID_UNIQUE` (`account_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (4,'2015-11-05 14:29:36'),(5,'2015-11-05 14:29:38'),(6,'2015-11-05 14:29:36'),(7,'2018-08-02 21:47:30'),(8,'2018-08-02 21:47:30'),(9,'2018-08-02 21:47:30'),(10,'2018-08-02 21:47:30'),(11,'2018-08-02 21:54:59'),(12,'2018-08-02 21:55:02'),(13,'2018-08-02 21:55:02'),(14,'2018-08-02 21:55:03'),(15,'2018-08-02 21:55:03'),(16,'2018-08-02 21:55:03'),(17,'2018-08-02 21:55:03'),(18,'2018-08-02 21:56:44'),(19,'2018-08-02 21:57:39'),(20,'2018-08-02 21:59:49'),(21,'2018-08-02 21:59:50'),(22,'2018-08-02 21:59:50'),(23,'2018-08-02 21:59:50'),(24,'2018-08-02 21:59:50'),(25,'2018-08-02 21:59:51'),(26,'2018-08-02 22:11:23'),(27,'2018-08-02 22:11:27'),(28,'2018-08-02 22:11:29'),(29,'2018-08-02 22:11:29'),(30,'2018-08-02 22:11:29'),(31,'2018-08-02 22:23:47'),(32,'2018-08-02 22:23:50'),(33,'2018-08-02 22:39:53'),(34,'2018-08-02 22:39:57'),(35,'2018-08-02 22:39:59'),(36,'2018-08-02 22:40:00'),(37,'2018-08-02 22:40:01'),(38,'2018-08-02 22:40:03'),(39,'2018-08-02 22:40:04'),(40,'2018-08-02 22:40:05'),(41,'2018-08-02 22:40:06'),(42,'2018-08-02 23:27:33'),(43,'2018-08-02 23:27:47'),(44,'2018-08-02 23:29:04'),(45,'2018-08-02 23:41:52'),(46,'2018-08-02 23:43:15');
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_devices`
--

DROP TABLE IF EXISTS `account_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `account_devices` (
  `device_id` varchar(200) NOT NULL,
  `account_id` bigint(20) NOT NULL,
  `model` varchar(50) DEFAULT NULL,
  `type` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `os_family` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  PRIMARY KEY (`device_id`),
  UNIQUE KEY `DEVICE_ID_UNIQUE` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_devices`
--

LOCK TABLES `account_devices` WRITE;
/*!40000 ALTER TABLE `account_devices` DISABLE KEYS */;
INSERT INTO `account_devices` VALUES ('51016D41-2C26-5C29-A7E7-71C986D2B2D5',45,'MacBookPro11,4','Desktop','MacOSX'),('asd',4,'doogee',NULL,'android'),('QWERTY',44,'doogee',NULL,'android'),('test_00',46,'MacBookPro11,4','Desktop','MacOSX');
/*!40000 ALTER TABLE `account_devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_users`
--

DROP TABLE IF EXISTS `account_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `account_users` (
  `account_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app_id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`account_id`,`app_id`),
  UNIQUE KEY `ACCOUNT_APP_RELATION_UNIQUE` (`account_id`,`app_id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_users`
--

LOCK TABLES `account_users` WRITE;
/*!40000 ALTER TABLE `account_users` DISABLE KEYS */;
INSERT INTO `account_users` VALUES (4,2,8),(4,3,9),(42,2,15),(43,2,16),(44,2,17),(45,2,18),(45,3,4),(46,2,19);
/*!40000 ALTER TABLE `account_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_sdv`
--

DROP TABLE IF EXISTS `meta_sdv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `meta_sdv` (
  `app_id` bigint(20) NOT NULL,
  `table_name` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `version_number` bigint(20) NOT NULL,
  PRIMARY KEY (`app_id`,`table_name`),
  UNIQUE KEY `ID_UNIQUE` (`app_id`,`table_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_sdv`
--

LOCK TABLES `meta_sdv` WRITE;
/*!40000 ALTER TABLE `meta_sdv` DISABLE KEYS */;
INSERT INTO `meta_sdv` VALUES (2,'meta_appconstant',15),(2,'meta_country',11),(2,'meta_star',16),(3,'meta_appconstant',4),(3,'meta_country',4);
/*!40000 ALTER TABLE `meta_sdv` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-13  3:50:14
