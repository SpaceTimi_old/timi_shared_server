<?php
/*
 * ~~~~~~~~~~ This must always be at the top ~~~~~~~~~~
 */
/**
 * Set up constant for app name and app id
 */
define("__APP_NAME", 'bonda');
require_once "app_ids.php";
define("__APP_ID", AppIds::$BONDA_APP_ID);
/**
 * Set up constants for app source code paths
 */
define("__APP_SRC_DIR", realpath(dirname(__FILE__) . '/../../' . __APP_NAME . "_server") . '/');

?>