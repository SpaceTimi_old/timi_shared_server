<?php

include_once __SHARED_SRC_DIR . "SDV/SDVManager.php";
include_once __SHARED_SRC_DIR . "Data/ServerResponseMessage.php";
include_once __SHARED_SRC_DIR . "Data/ServerRequestMessage.php";
include_once __SHARED_SRC_DIR . "Login/LoginHandler.php";
include_once __SHARED_SRC_DIR . "Login/LoginRequestData.php";
include_once __SHARED_SRC_DIR . "ChangeEvent/ChangeEventHandler.php";
include_once __SHARED_SRC_DIR . "ChangeEvent/IChangeEventHandler.php";
include_once __SHARED_SRC_DIR . "ChangeEvent/ChangeEventRequestData.php";
include_once __SHARED_SRC_DIR . "ChangeEventHandlers/SharedChangeEventHandlers.php";
include_once __SHARED_SRC_DIR . "Account/AccountHelper.php";
include_once __APP_SRC_DIR . "AppApi.php";
include_once __APP_SRC_DIR . "ChangeEventHandlers/AppChangeEventHandlers.php";

function ProcessAPIRequest() {
    /** Register change event handlers */
    SharedChangeEventHandlers::Register();
    AppChangeEventHandlers::Register();

    /** @var ServerRequestMessage $serverRequestMessage */
    $serverRequestMessage = json_decode(idx($_POST, 'serverRequestMessageJson'));
    Context::GetContext()->requestMessage = $serverRequestMessage;

    $requestType = $serverRequestMessage->requestType;
    $requestDataJson = $serverRequestMessage->requestDataJson;
    switch ($requestType) {
        case 'login': {
            Context::GetContext()->apiRequestType = Context::API_REQUEST_TYPE_LOGIN;

            /** @var LoginRequestData $loginRequestData */
            $loginRequestData = json_decode($requestDataJson);
            LoginHandler::ProcessLoginRequest($loginRequestData);
            break;
        }
        case 'changeEvent': {
            Context::GetContext()->apiRequestType = Context::API_REQUEST_TYPE_CHANGE_EVENT;

            /** @var ChangeEventRequestData $changeEventRequestData */
            $changeEventRequestData = json_decode($requestDataJson);
            ChangeEventHandler::ProcessChangeEventRequest($changeEventRequestData);
            break;
        }
        default: {
            Log::LogNotice("Unknown message type in api request. Request: " . print_r($serverRequestMessage, true));
        }
    }

}

function GenerateResponseWithDataModels() {

    /** @var DataModelBase[] $dataModels */
    $dataModels = array_merge(GetSharedDataModels(),
                              GetSharedUserDataModels(),
                              GetAppDataModels(),
                              GetAppUserDataModels());

    $serverResponseMessage = new ServerResponseMessage();
    $serverResponseMessage->prepareMessage(Context::GetContext()->account->account_id,
                                           Context::GetContext()->user->user_id,
                                           $dataModels);

    echo json_encode($serverResponseMessage);
}

/**
 * @return DataModelBase[]
 */
function GetSharedDataModels() {
    $dataModels = array();

    // Add data models here

    return $dataModels;
}

/**
 * @return DataModelBase[]
 */
function GetSharedUserDataModels() {
    $dataModels = array();

    {
        $userSdvListDataModel = new UserSDVListDataModel();
        $allSdvs = array_merge(SDVManager::GetAllAppSDVs(), SDVManager::GetAllSharedSDVs());
        foreach ($allSdvs as $tableName => $versionNumber) {
            $userSdvListDataModel->userSdvDatas[] = new UserSDVData($tableName, $versionNumber);
        }
        $dataModels[] = $userSdvListDataModel;
    }

    return $dataModels;
}

?>



