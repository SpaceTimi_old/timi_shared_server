<?php


/**
 * Parse command line arguments
 */
if ($argc < 5) {
    PrintUsage(); die;
}
$appId = $argv[1];
$databaseName = $argv[2];
$sourceEnv = $argv[3];
$destinationEnv = $argv[4];
$tables = array();
for ($i = 5; $i < $argc; ++$i) {
    $tables[] = $argv[$i];
}

/**
 * Set up constant for app name
 */
define("__APP_NAME", 'shared');
define("__APP_ID", $appId);
/**
 * Set up constants for app source code paths
 */
define("__APP_SRC_DIR", realpath(dirname(__FILE__) . '/../../../../' . __APP_NAME . "_server") . '/');
require_once "../../shared_init.php";
Init(__APP_ID, __APP_NAME, Context::CONTEXT_TYPE_TOOL);


/**
 * Read config file
 */
$dbConfig = json_decode(file_get_contents("db_config.json"), true);
$databases = idx($dbConfig, 'DATABASES');
$environments = idx($dbConfig, 'ENVIRONMENTS');

/**
 * Make sure args are ok
 */
if (!in_array($databaseName, $databases)) {
    echo "Invalid database name\n";
    PrintUsage(); die;
}
if (!in_array($sourceEnv, $environments)) {
    echo "Invalid source environment\n";
    PrintUsage(); die;
}
if (!in_array($destinationEnv, $environments)) {
    echo "Invalid destination environment\n";
    PrintUsage(); die;
}

/**
 * Get database connection credentials for source and destination environments
 */
$dbCreds = idxidx($dbConfig, 'DETAILS', $databaseName, null);
$srcCreds = idx($dbCreds, $sourceEnv);
$destCreds = idx($dbCreds, $destinationEnv);

/**
 * Establish source database connection and select db
 */
$srcConn = mysqli_connect($srcCreds['DB_SERVER'], $srcCreds['DB_USERNAME'], $srcCreds['DB_PASSWORD'], $databaseName);
if (mysqli_connect_errno()) {
    echo "Failed to connect to source MySQL: " . mysqli_connect_error();
    die;
}
/**
 * Establish destination database connection and select db
 */
$destConn = mysqli_connect($destCreds['DB_SERVER'], $destCreds['DB_USERNAME'], $destCreds['DB_PASSWORD'], $databaseName);
if (mysqli_connect_errno()) {
    echo "Failed to connect to destination MySQL: " . mysqli_connect_error();
    die;
}

echo "Copying $databaseName DB from $sourceEnv to $destinationEnv:\n";

foreach ($tables as $table) {
    echo "\tTable $table:\n";

    /**
     * Construct and run query to truncate destination table
     */
    echo "\t\tTruncating in $destinationEnv\n";
    $query = "truncate table $table";
    $result = mysqli_query($destConn, $query);
    if (!$result) {
        echo "Error running query: " . $query. "\n"; continue;
    }

    /**
     * Construct and run query to get everything from source
     */
    echo "\t\tFetching from $sourceEnv\n";
    $query = "select * from $table";
    $result = mysqli_query($srcConn, $query);
    if (!$result) {
        echo "Error running query: " . $query. "\n"; continue;
    }

    /**
     * Construct fields list for insert into destination
     */
    $fields = array();
    $resultFields = $result->fetch_fields();
    foreach ($resultFields as $resultField) {
        $fields[] = $resultField->name;
    }
    $fieldsClause = implode(', ', $fields);

    /**
     * Construct list of values for insert into destination
     */
    $valuesList = array();
    foreach ($result as $row) {
        $values = array();
        foreach ($row as $field => $value) {
            $values[] = '\'' . addslashes($value) . '\'';
        }
        $valuesList[] = '(' . implode(', ', $values) . ')';
    }
    $valuesClause = implode(',', $valuesList);

    /**
     * Construct and run query to insert data into destination
     */
    echo "\t\tInserting into $destinationEnv\n";
    $query = "insert into $table ($fieldsClause) values $valuesClause";
    $result = mysqli_query($destConn, $query);
    if (!$result) {
        echo "Error running query: " . $query. "\n"; continue;
    }

    echo "\t\tDone\n";
}

function PrintUsage() {
    echo "Usage: php copy_tables.php <app_id> <database_name> <source_environment> <destination_environment> <list of tables>\n";
}
?>
