'use strict';

angular.
module('dirtyTables').
component('dirtyTables', {
    templateUrl: 'src/dirty-tables-feature/dirty-tables.template.html',
    controller: function DirtyTablesController($http) {
        var self = this;

        self.appTables = {
            'std': [
                'meta_country',
                'meta_appconstant',
                'meta_book_detail'
            ],
            'bonda': [
                'meta_country',
                'meta_appconstant',
                'meta_star'
            ],
            'shared': [
            ]
        };

        self.appName = 'std';
        self.selectedTables = [];
        self.setDirtyResults = [];
        self.setDirtyErrors = [];

        self.onSetDirty = function setDirty() {
            self.setDirtyResults = [];
            self.setDirtyErrors = [];

            if (self.selectedTables.length == 0) {
                self.setDirtyErrors.push('No tables selected');
                return;
            }

            $http({
                method: 'POST',
                url: 'dirty_api.php',
                data: {
                    'appName': self.appName,
                    'tableNames': JSON.stringify(self.selectedTables)
                },
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function (response) {
                self.setDirtyResults = angular.fromJson(response.data);
            });
        };

    }
});
