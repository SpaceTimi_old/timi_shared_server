<?php


$params = json_decode(file_get_contents('php://input'), true);

$appName = '';
$tableNames = '';

if (isset($params['appName'])) {
    $appName = $params['appName'];
}
if (isset($params['tableNames'])) {
    $tableNames = json_decode($params['tableNames'], true);
}

if (empty($appName)) {
    echo json_encode("Error! No app name set");
    die;
}
if (empty($tableNames)) {
    echo json_encode("Error! No tables specified");
    die;
}

require_once "../../app_ids.php";
$appId = AppIds::GetAppIdForApp($appName);
if (!isset($appId)) {
    echo json_encode("Error! Unknown app name " . $appName);
    die;
}

include_once '../../shared_init.php';
include '../../' . $appName . '_init.php';

Init(__APP_ID, $appName, Context::CONTEXT_TYPE_TOOL);
include_once __SHARED_SRC_DIR . "SDV/SDVManager.php";

$result = array();
/** @var string $tableName */
foreach ($tableNames as $tableName) {
    $newSdv = -1;
    if ($appName == 'shared') {
        $newSdv = SDVManager::UpdateSharedSDVForTable($tableName);
    } else {
        $newSdv = SDVManager::UpdateAppSDVForTable($tableName);
    }
    $result[] = "Successfully updated SDV for $tableName to $newSdv";
}

echo json_encode($result);

