<html lang="en">

<head>

    <title>Dirty</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>

    <style>
        .jumbotron {
            background-color: #b3ff99;
            color: #444488;
        }
    </style>


</head>

<body>

<br/>
<div class="container">
    <div class="jumbotron">

<?php

if (isset($_POST['app_name'])) {
    $appName = $_POST['app_name'];

    include_once '../shared_init.php';
    if ($appName != 'shared') {
        include '../' . $appName . '_init.php';
    } else {
        require_once "../app_ids.php";
        define("__APP_ID", AppIds::$SHARED_APP_ID);
    }

    Init(__APP_ID, $appName, Context::CONTEXT_TYPE_TOOL);
    include_once __SHARED_SRC_DIR . "SDV/SDVManager.php";

    $tableNames = array();
    if (empty($_POST['table_names'])) {
        echo '<p class="text-warning">No tables specified, dirtying everything.</p>';
        if ($appName == 'shared') {
            $tableNames = array();
        } else if ($appName == 'bonda') {
            $tableNames = array(
                'meta_star',
                'meta_country',
                'meta_appconstant'
            );
        } else if ($appName == 'std') {
            $tableNames = array(
                'meta_country',
                'meta_appconstant'
            );
        }
    } else {
        $tableNames = explode(',', preg_replace('/\s+/', '', $_POST['table_names']));
    }

    echo "Updating SDVs for $appName:";
    echo "<br/>";
    echo '<p class="text-info">';

    /** @var string $tableName */
    foreach ($tableNames as $tableName) {
        $newSdv = -1;
        if ($appName == 'shared') {
            $newSdv = SDVManager::UpdateSharedSDVForTable($tableName);
        } else {
            $newSdv = SDVManager::UpdateAppSDVForTable($tableName);
        }
        echo "Successfully updated " . $tableName . " sdv to $newSdv <br/>";
    }
    echo '</p>';
    echo "<hr/>";
}

?>

    <form action="dirty.php" method="post">
        <div class="row">
            <div class="col-md-2 col-sm-3"> App name: </div>
            <div class="col-md-4 col-sm-6">
                <select class="text-primary" name="app name">
                    <option value="std">Save The Date</option>
                    <option value="bonda">Bonda</option>
                    <option value="shared">Shared</option>
                </select>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-2 col-sm-3">
                Tables (comma separated):
            </div>
            <div class="col-md-8 col-sm-6">
                <input type="text" class="form-control" name="table_names" size="100"/>
            </div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-2">
                <input type="submit" class="btn btn-danger" value="Set Dirty"/>
            </div>
        </div>
    </form>

</div>
</div>

</body>
</html>
