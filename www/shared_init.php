<?php

/**
 * Set up constants for shared source code paths
 */
defined('__SHARED_SRC_DIR') or
define("__SHARED_SRC_DIR", realpath(dirname(__FILE__) . '/../src') . '/');

defined('__THIRD_PARTY_DIR') or
define("__THIRD_PARTY_DIR", __SHARED_SRC_DIR . "../ThirdParty/");

/**
 * Include necessary utilities
 */
include_once __SHARED_SRC_DIR . "Core/DBConnectionManager.php";
include_once __SHARED_SRC_DIR . "Core/RedisConnectionManager.php";
include_once __SHARED_SRC_DIR . "Core/CoreUtilities.php";
include_once __SHARED_SRC_DIR . "Core/DataMapper.php";
include_once __SHARED_SRC_DIR . "Core/Context.php";
include_once __SHARED_SRC_DIR . "Data/DataModelBase.php";
include_once __SHARED_SRC_DIR . "Utilities/ErrorReporting.php";

function Init($appId, $appName, $contextType) {
    /**
     * Set context
     * This must always be done first
     */
    $context = Context::GetContext();
    $context->appId = $appId;
    $context->appName = $appName;
    $context->contextType = $contextType;

    /**
     * Load the shared config file and verify contents
     */
    $sharedConfig = json_decode(file_get_contents(__SHARED_SRC_DIR . "Config/SharedConfig.json"), true);
    if (!isset($sharedConfig) ||
        !isset($sharedConfig["CURRENT_ENVIRONMENT"]) ||
        !isset($sharedConfig["ENVIRONMENTS"])) {
        die ("Problematic shared config");
    }

    /**
     * Set Current App Environment and verify that it is defined
     */
    $context->currentEnvironment = $sharedConfig["CURRENT_ENVIRONMENT"];
    if (!isset($sharedConfig["ENVIRONMENTS"][$context->currentEnvironment])) {
        die ("Current environment not defined in shared config");
    }

    /**
     * Configure error reporting based on current environment
     */
    ConfigureErrorReporting();

    /**
     * Create database connection params for shared DB
     */
    $sharedDbConfig = $sharedConfig["ENVIRONMENTS"][$context->currentEnvironment]["SHARED_DB"];
    define("__SHARED_DATABASE", $sharedDbConfig["DB_DATABASE"]);
    DBConnectionManager::SetParamsForConnection($sharedDbConfig["DB_SERVER"],
        $sharedDbConfig["DB_USERNAME"],
        $sharedDbConfig["DB_PASSWORD"], __SHARED_DATABASE);

    /**
     * Create redis connection params
     */
    $redisConfig = $sharedConfig["ENVIRONMENTS"][$context->currentEnvironment]["REDIS"];
    RedisConnectionManager::SetConnectionParams($redisConfig["SCHEME"],
        $redisConfig["HOST"],
        $redisConfig["PORT"]);

    if ($context->appName != 'shared') {

        /**
         * Load the app config file for the current app and verify contents
         */
        $appConfig = json_decode(file_get_contents(__APP_SRC_DIR . "Config/AppConfig.json"), true);
        if (!isset($appConfig) ||
            !isset($appConfig["ENVIRONMENTS"])) {
            die ("Problematic app config");
        }

        /**
         * Verify current app environment is defined
         */
        if (!isset($appConfig["ENVIRONMENTS"][$context->currentEnvironment])) {
            die ("Current environment not defined in app config");
        }

        /**
         * Create database connection params for app DB
         */
        $appDbConfig = $appConfig["ENVIRONMENTS"][$context->currentEnvironment]["APP_DB"];
        define("__APP_DATABASE", $appDbConfig["DB_DATABASE"]);
        DBConnectionManager::SetParamsForConnection($appDbConfig["DB_SERVER"],
            $appDbConfig["DB_USERNAME"],
            $appDbConfig["DB_PASSWORD"], __APP_DATABASE);
    }
}

?>
