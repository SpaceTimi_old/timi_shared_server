<?php

class AppIds {
    /** @var int */
    public static $SHARED_APP_ID        = 1;
    /** @var int */
    public static $BONDA_APP_ID         = 2;
    /** @var int */
    public static $STD_APP_ID           = 3;

    static function GetAppIdForApp($appName) {
        switch ($appName) {
            case 'shared': return self::$SHARED_APP_ID;
            case 'bonda': return self::$BONDA_APP_ID;
            case 'std': return self::$STD_APP_ID;
        }

        return null;
    }
}
