<?php

include_once __SHARED_SRC_DIR . "Account/Tables/Account.php";
include_once __SHARED_SRC_DIR . "Account/Tables/AccountDevice.php";
include_once __SHARED_SRC_DIR . "Account/Tables/AccountUser.php";
include_once __SHARED_SRC_DIR . "Account/Tables/User.php";

class AccountHelper {

    /**
     * @param $deviceId string
     * @return Account|null
     */
    public static function GetAccountForDevice($deviceId) {
        $accountDevice = AccountDevice::loadByDeviceId($deviceId);
        if (empty($accountDevice)) {
            return null;
        } else {
            $accountId = $accountDevice->account_id;

            $account = Account::loadByAccountId($accountId);
            if (empty($account)) {
                Log::LogAndTriggerFatalError("No such account for account id: $accountId, device id: $deviceId");
                return null;
            }

            return $account;
        }
    }

    /**
     * @param $deviceId string
     * @param $model string
     * @param $os string
     * @return Account|null
     */
    public static function CreateNewAccountForDevice($deviceId, $model, $type, $os) {
        $account = Account::createNew();
        if (!empty($account)) {
            Log::LogMessage("Created new account: $account->account_id");
        } else {
            Log::LogNotice("Error creating new account");
            return null;
        }

        $accountId = $account->account_id;
        $accountDevice = AccountDevice::createNew($deviceId, $accountId, $model, $type, $os);
        if (!empty($accountDevice)) {
            Log::LogMessage("Registered device: $deviceId with account: $accountId");
        } else {
            Log::LogNotice("Error registering device: $deviceId with account: $accountId");
        }

        return $account;
    }

    /**
     * @param $accountId int
     * @param $appId int
     * @return null|User
     */
    public static function GetUserForAccount($accountId, $appId) {
        $accountUser = AccountUser::loadByAccountIdAppId($accountId, $appId);
        if (empty($accountUser)) {
            return null;
        } else {
            $userId = $accountUser->user_id;
            $user = User::loadByUserId($userId);
            if (empty($user)) {
                Log::LogAndTriggerFatalError("No such user for app id: $appId, user id: $userId, account id: $accountId, device id: $deviceId");
                return null;
            }
            return $user;
        }
    }

    /**
     * @param $accountId int
     * @return null|User
     */
    public static function CreateNewUserForAccount($accountId, $appId) {
        $user = User::createNew();
        Log::LogMessage("Created new user: $user->user_id");
        $accountUser = AccountUser::createNew($accountId, $appId, $user->user_id);
        if (empty($accountUser)) {
            Log::LogNotice("Error associating user: $user->user_id with account: $accountId");
            return null;
        }

        return $user;
    }
}
