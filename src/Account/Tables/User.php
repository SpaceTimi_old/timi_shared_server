<?php

include_once __SHARED_SRC_DIR."Core/RedisObjectBase.php";
include_once __SHARED_SRC_DIR."Core/DataMapper.php";

class User extends RedisObjectBase {

    /** @var int */
    public $user_id;

    /** @var DateTime */
    public $created_time;

    /** @var DateTime */
    public $last_login_time;

    /**
     * @param $userId int
     * @return User
     */
    public static function loadByUserId($userId) {
        $user = new User();
        RedisObjectBase::loadByPrimaryKeys(array($userId), $user);

        return $user;
    }

    /**
     * @return User
     */
    public static function createNew() {
        $user = new User();
        $user->user_id = null;
        $user->created_time = getDateTimeNow();
        $user->last_login_time = getDateTimeNow();
        $user->Apply();

        return $user;
    }

    public static function GetClassName() {
        return get_class();
    }

    protected function getDataMapper() {
        return DataMapper::GetDataMapper(self::GetClassName());
    }

    protected function getRedisKeyPrefix() {
        return 'v0';
    }
}

DataMapper::AddDataMapper(User::GetClassName(),
    new DataMapper(__APP_DATABASE,
        'user',
        true,
        User::GetClassName(),
        array('user_id'),
        function () {
            return (Context::GetContext()->contextType == Context::CONTEXT_TYPE_API ||
                    Context::GetContext()->contextType == Context::CONTEXT_TYPE_TOOL);
        },
        'user_id'));
?>
