<?php

include_once __SHARED_SRC_DIR."Core/DBObjectBase.php";
include_once __SHARED_SRC_DIR."Core/DataMapper.php";

class AccountDevice extends RedisObjectBase {

    /** @var string */
    public $device_id;
    /** @var int */
    public $account_id;
    /** @var string */
    public $model;
    /** @var string */
    public $type;
    /** @var string */
    public $os_family;

    /**
     * @param $deviceId string
     * @return AccountDevice
     */
    public static function loadByDeviceId($deviceId) {
        $accountDevice = new AccountDevice();
        RedisObjectBase::loadByPrimaryKeys(array($deviceId), $accountDevice);

        return $accountDevice;
    }

    /**
     * @param $deviceId string
     * @param $accountId int
     * @param $model string
     * @param $os_family string
     * @return AccountDevice
     */
    public static function createNew($deviceId, $accountId, $model, $type, $os_family) {
        $accountDevice = new AccountDevice();
        $accountDevice->device_id = $deviceId;
        $accountDevice->account_id = $accountId;
        $accountDevice->model = $model;
        $accountDevice->type = $type;
        $accountDevice->os_family = $os_family;
        $accountDevice->Apply();

        return $accountDevice;
    }

    public static function GetClassName() {
        return get_class();
    }

    protected function getDataMapper() {
        return DataMapper::GetDataMapper(self::GetClassName());
    }

    protected function getRedisKeyPrefix() {
        return 'v0';
    }
}

DataMapper::AddDataMapper(AccountDevice::GetClassName(),
    new DataMapper(__SHARED_DATABASE,
        'account_devices',
        true,
        AccountDevice::GetClassName(),
        array('device_id'),
        function () {
            return (Context::GetContext()->contextType == Context::CONTEXT_TYPE_API ||
                    Context::GetContext()->contextType == Context::CONTEXT_TYPE_TOOL);
        }));
?>
