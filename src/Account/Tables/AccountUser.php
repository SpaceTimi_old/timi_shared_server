<?php

include_once __SHARED_SRC_DIR."Core/DBObjectBase.php";
include_once __SHARED_SRC_DIR."Core/DataMapper.php";

class AccountUser extends RedisObjectBase {

    /** @var int */
    public $account_id;
    /** @var int */
    public $app_id;
    /** @var int */
    public $user_id;

    /**
     * @param $accountId int
     * @param $appId int
     * @return AccountUser
     */
    public static function loadByAccountIdAppId($accountId, $appId) {
        $accountUser = new AccountUser();
        RedisObjectBase::loadByPrimaryKeys(array($accountId, $appId), $accountUser);

        return $accountUser;
    }

    /**
     * @param $accountId int
     * @param $appId int
     * @param $userId int
     * @return AccountUser
     */
    public static function createNew($accountId, $appId, $userId) {
        $accountUser = new AccountUser();
        $accountUser->account_id = $accountId;
        $accountUser->user_id = $userId;
        $accountUser->app_id = $appId;
        $accountUser->Apply();

        return $accountUser;
    }

    public static function GetClassName() {
        return get_class();
    }

    protected function getDataMapper() {
        return DataMapper::GetDataMapper(self::GetClassName());
    }

    protected function getRedisKeyPrefix() {
        return 'v0';
    }
}

DataMapper::AddDataMapper(AccountUser::GetClassName(),
    new DataMapper(__SHARED_DATABASE,
        'account_users',
        true,
        AccountUser::GetClassName(),
        array('account_id', 'app_id'),
        function () {
            return (Context::GetContext()->contextType == Context::CONTEXT_TYPE_API ||
                    Context::GetContext()->contextType == Context::CONTEXT_TYPE_TOOL);
        }));
?>
