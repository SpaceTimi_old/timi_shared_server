<?php

include_once __SHARED_SRC_DIR."Core/RedisObjectBase.php";
include_once __SHARED_SRC_DIR."Core/DataMapper.php";

class Account extends RedisObjectBase {

    /** @var int */
    public $account_id;

    /** @var DateTime */
    public $created_time;

    /**
     * @param $accountId int
     * @return Account
     */
    public static function loadByAccountId($accountId) {
        $account = new Account();
        RedisObjectBase::loadByPrimaryKeys(array($accountId), $account);

        return $account;
    }

    /**
     * @return Account
     */
    public static function createNew() {
        $account = new Account();
        $account->account_id = null;
        $account->created_time = getDateTimeNow();
        $account->Apply();

        return $account;
    }

    public static function GetClassName() {
        return get_class();
    }

    protected function getDataMapper() {
        return DataMapper::GetDataMapper(self::GetClassName());
    }

    protected function getRedisKeyPrefix() {
        return 'v0';
    }
}

DataMapper::AddDataMapper(Account::GetClassName(),
    new DataMapper(__SHARED_DATABASE,
        'account',
        true,
        Account::GetClassName(),
        array('account_id'),
        function () {
            return (Context::GetContext()->contextType == Context::CONTEXT_TYPE_API ||
                    Context::GetContext()->contextType == Context::CONTEXT_TYPE_TOOL);
        },
        'account_id'));
?>
