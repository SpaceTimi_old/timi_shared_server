<?php

class Log {

    private static $PREPEND_STRING_DEBUG_MESSAGE    = 'spacetimi dbglog message: ';
    private static $PREPEND_STRING_NOTICE           = 'spacetimi notice: ';
    private static $PREPEND_STRING_WARNING          = 'spacetimi warning: ';
    private static $PREPEND_STRING_FATAL_ERROR      = 'spacetimi fatal error: ';

    /**
     * Prints a variable to error_log. Similar to var_dump
     * Ignored on prod
     * @param $message string
     */
    public static function LogVar($var, $message = '') {
        if (Context::GetContext()->currentEnvironment != Context::CURRENT_ENVIRONMENT_PROD) {
            self::LogMessage($message .
                (empty($message) ? '' : ': ') .
                print_r($var, true));
        }
    }

    /**
     * Logs a simple message to error_log
     * Ignored on prod
     * @param $message string
     */
    public static function LogMessage($message) {
        if (Context::GetContext()->currentEnvironment != Context::CURRENT_ENVIRONMENT_PROD) {
            error_log(self::$PREPEND_STRING_DEBUG_MESSAGE . $message);
        }
    }

    /**
     * Logs a notice to error log
     * Works on all environments
     * @param $message string
     */
    public static function LogNotice($message) {
        trigger_error(self::$PREPEND_STRING_NOTICE . $message, E_USER_NOTICE);
    }

    /**
     * Logs a warning to error log
     * Works on all environments
     * @param $message string
     */
    public static function LogWarning($message) {
        trigger_error(self::$PREPEND_STRING_WARNING . $message, E_USER_WARNING);
    }

    /**
     * Only use this for actual *fatal errors* since triggering a fatal error will stop script execution
     * For example, use this when continuing with script execution will likely cause data corruption
     * Works on all environments
     * @param $message string
     */
    public static function LogAndTriggerFatalError($message) {
        trigger_error(self::$PREPEND_STRING_FATAL_ERROR . $message, E_USER_ERROR);
    }

    /**
     * Will trigger fatal error and stop script execution
     * For example, use this when continuing with script execution will likely cause data corruption
     * Works on all environments
     * @param $message string
     */
    public static function SyncError($message, $objects) {
        $kvps = array();
        foreach ($objects as $key => $value) {
            $kvps[] = "{ $key = $value }";
        }
        $relevantData = "Relevant data: " . implode($kvps);

        $userId = '';
        if (!empty(Context::GetContext()->user)) {
            $userId = Context::GetContext()->user->user_id;
        }
        $accountId = '';
        if (!empty(Context::GetContext()->account)) {
            $accountId = Context::GetContext()->account->account_id;
        }

        $requestMessage = '';
        if (!empty(Context::GetContext()->requestMessage)) {
            $requestMessage = " Request message: " . print_r(Context::GetContext()->requestMessage, true);
        }

        $prefix = "Sync error";
        switch (Context::GetContext()->apiRequestType) {
            case Context::API_REQUEST_TYPE_LOGIN:
                $prefix .= " during login.";
                $message = "$prefix {user id: $userId, account id: $accountId} $message $relevantData $requestMessage";
                break;
            case Context::API_REQUEST_TYPE_CHANGE_EVENT:
                $prefix .= " processing change event.";
                $message = "$prefix {user id: $userId, account id: $accountId} $message $relevantData $requestMessage";
                break;
            default: $prefix .= ". ";
                $message = "$prefix {user id: $userId, account id: $accountId} $message $relevantData $requestMessage";
        }

        // TODO: Store sync errors in DB
        self::LogAndTriggerFatalError($message);
    }
}

/**
 * Configure error reporting
 * No errors on prod, all errors on dev and staging
 */
function ConfigureErrorReporting() {
    // First turn on all error reporting
    error_reporting(E_ALL);

    $currentEnvironment = Context::GetContext()->currentEnvironment;
    if ($currentEnvironment == Context::CURRENT_ENVIRONMENT_PROD) {
        ini_set('display_errors', '0');
    } else {
        ini_set('display_errors', '1');
    }
}
?>
