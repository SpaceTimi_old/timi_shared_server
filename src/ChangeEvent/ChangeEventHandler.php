<?php

class ChangeEventHandler {

    /**
     * @var array of change event type => change event handler
     */
    private static $_changeEventHandlersByType;

    /**
     * @param $changeEventRequestData ChangeEventRequestData
     */
    public static function ProcessChangeEventRequest($changeEventRequestData) {
        if (empty($changeEventRequestData->changeEventType)) {
            Log::LogNotice("Change event type not set. Request: " . print_r($changeEventRequestData, true));
            return;
        }

        $account = Account::loadByAccountId($changeEventRequestData->accountId);
        if (empty($account)) {
            Log::SyncError("No such account",
                array('user id' => $changeEventRequestData->userId,
                    'account id' => $changeEventRequestData->accountId));
        }
        Context::GetContext()->account = $account;

        $user = User::loadByUserId($changeEventRequestData->userId);
        if (empty($user)) {
            Log::SyncError("No such user",
                array('user id' => $changeEventRequestData->userId,
                    'account id' => $changeEventRequestData->accountId));
        }
        Context::GetContext()->user = $user;

        Log::LogMessage("Change event received from user id: $user->user_id, account id: $account->account_id. Change event data: " . print_r($changeEventRequestData, true));

        /** @var IChangeEventHandler $handler */
        $handler = idx(self::$_changeEventHandlersByType, $changeEventRequestData->changeEventType, null);
        if (!isset($handler)) {
            Log::LogNotice("No change event handler registered for type: " . $changeEventRequestData->changeEventType);
            return;
        }

        $handler->HandleChangeEvent(json_decode($changeEventRequestData->changeEventDataJson));
    }

    public static function RegisterChangeEventHandlers($handlers) {
        /**
         * @var string $type
         * @var IChangeEventHandler $handler
         */
        foreach ($handlers as $handler) {
            self::$_changeEventHandlersByType[$handler->GetChangeEventType()] = $handler;
        }
    }



}

?>

