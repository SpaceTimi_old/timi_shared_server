<?php

class ChangeEventRequestData {
    /** @var int */
    public $userId;

    /** @var int */
    public $accountId;

    /** @var string */
    public $changeEventType;

    /** @var string */
    public $changeEventDataJson;
}

?>

