<?php

interface IChangeEventHandler {

    /**
     * @return string
     */
    function GetChangeEventType();

    /**
     * @param $changeEventData mixed
     */
    function HandleChangeEvent($changeEventData);

}

?>

