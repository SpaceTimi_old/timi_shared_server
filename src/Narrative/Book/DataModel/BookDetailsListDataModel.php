<?php

include_once __SHARED_SRC_DIR . "Narrative/Book/BookDetailList.php";
include_once __SHARED_SRC_DIR . "Narrative/Book/DataModel/BookDetailData.php";

class BookDetailsListDataModel extends DataModelBase {
    /** @var BookDetailData[] */
    public $bookDetails;

    public function prepareData() {
        $bookDetails = BookDetailList::GetInstance()->GetBookDetailsList();
        /** @var BookDetail $bookDetail */
        foreach ($bookDetails as $bookDetail) {
            $bookDetailData = new BookDetailData($bookDetail->id, $bookDetail->display_title, $bookDetail->file_name, $bookDetail->book_json_representation);
            $this->bookDetails[] = $bookDetailData;
        }
    }

    #region IDataModelDescriptor
    /**
     * @return string[]
     */
    public function GetDependencyTableNames() {
        return array(DataMapper::GetDataMapper(BookDetail::GetClassName())->tableName);
    }

    /**
     * @return bool
     */
    public function IsUserDataModel() {
        return false;
    }

    /**
     * @return int
     */
    public function GetAppId() {
        return __APP_ID;
    }
    #endregion
}

?>
