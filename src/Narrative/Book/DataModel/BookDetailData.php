<?php

class BookDetailData {
    /** @var int */
    public $id;

    /** @var string */
    public $display_title;

    /** @var string */
    public $file_name;

    /** @var string */
    public $book_json_representation;

    function __construct($id, $display_title, $file_name, $book_json_representation) {
        $this->id = $id;
        $this->display_title = $display_title;
        $this->file_name = $file_name;
        $this->book_json_representation = $book_json_representation;
    }

}

?>