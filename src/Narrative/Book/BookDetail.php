<?php

include_once __SHARED_SRC_DIR . "Core/DBObjectBase.php";
include_once __SHARED_SRC_DIR . "Core/DataMapper.php";

class BookDetail extends DBObjectBase {

    public $id;
    public $display_title;
    public $file_name;
    public $book_json_representation;

    public static function GetClassName() {
        return get_class();
    }

    protected function getDataMapper() {
        return DataMapper::GetDataMapper(self::GetClassName());
    }
}

DataMapper::AddDataMapper(BookDetail::GetClassName(),
new DataMapper(__APP_DATABASE,
        'meta_book_detail',
        false,
        BookDetail::GetClassName(),
        array('id'),
        function () {
            return (Context::GetContext()->contextType == Context::CONTEXT_TYPE_TOOL);
        }));
