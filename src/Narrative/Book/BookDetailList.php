<?php

include_once __SHARED_SRC_DIR . "Core/RedisObjectListBase.php";
include_once __SHARED_SRC_DIR . "Narrative/Book/BookDetail.php";

class BookDetailList extends RedisObjectListBase {

    /** @var BookDetailList */
    private static $_instance;

    /**
     * @return BookDetailList
     */
    public static function GetInstance() {
        if (!isset(self::$_instance)) {
            self::$_instance = new BookDetailList();
        }
        return self::$_instance;
    }

    public function GetBookDetailsList() {
        return $this->getList();
    }

    /**
     * @param $id string
     * @return BookDetail|null
     */
    public function GetBookDetailById($id) {
        $list = $this->getList();
        /** @var BookDetail $bookDetail */
        foreach ($list as $bookDetail) {
            if ($bookDetail->id == $id) {
                return $bookDetail;
            }
        }
        return null;
    }

    /**
     * @return DataMapper
     */
    protected function getDataMapper() {
        return DataMapper::GetDataMapper(BookDetail::GetClassName());
    }

    protected function getRedisKeyPrefix() {
        return 'v0';
    }
}