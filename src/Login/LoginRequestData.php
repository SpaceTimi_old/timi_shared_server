<?php

include_once __SHARED_SRC_DIR . "SDV/UserSDVListDataModel.php";

class LoginRequestData {

    /** @var string */
    public $deviceId;

    /** @var string */
    public $deviceType;

    /** @var string */
    public $deviceModel;

    /** @var string */
    public $deviceOSFamily;

    /** @var UserSDVListDataModel */
    public $userSdvListDataModel;
}

?>

