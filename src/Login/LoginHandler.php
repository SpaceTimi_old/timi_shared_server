<?php

class LoginHandler {

    /**
     * @param $loginRequestData LoginRequestData
     */
    public static function ProcessLoginRequest($loginRequestData) {
        Log::LogMessage("Login received: " . print_r($loginRequestData, true));

        $account = AccountHelper::GetAccountForDevice($loginRequestData->deviceId);
        if (empty($account)) {
            $account = AccountHelper::CreateNewAccountForDevice($loginRequestData->deviceId,
                                                                 $loginRequestData->deviceModel,
                                                                 $loginRequestData->deviceType,
                                                                 $loginRequestData->deviceOSFamily);
        }
        Context::GetContext()->account = $account;

        $user = AccountHelper::GetUserForAccount($account->account_id, __APP_ID);
        if (empty($user)) {
            $user = AccountHelper::CreateNewUserForAccount($account->account_id, __APP_ID);
        } else {
            $user->last_login_time = getDateTimeNow();
            // TODO: Do this somewhere else at the end of api request
            $user->Apply();
        }
        Context::GetContext()->user = $user;

        Context::GetContext()->SetUserSdvData($loginRequestData->userSdvListDataModel);

        GenerateResponseWithDataModels();
    }

}

?>

