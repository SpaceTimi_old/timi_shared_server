<?php

require_once __THIRD_PARTY_DIR . "predis/autoload.php";

class RedisConnectionManager {

    /**
     * Private constructor. Use static methods
     */
    private function __construct() { }

    /**
     * @var \Predis\Client
     */
    private static $_connection;

    /**
     * @var array
     */
    private static $_connectionParams;

    /**
     * @param $scheme string
     * @param $host string
     * @param $port int
     */
    public static function SetConnectionParams($scheme, $host, $port) {
        if (empty($scheme) || empty($host) || empty($port)) {
            Log::LogWarning("Redis connection parameters not valid");
            return;
        }
        self::$_connectionParams = array(
            'scheme' => $scheme,
            'host' => $host,
            'port' => $port
        );
    }

    /**
     * @return null|\Predis\Client
     */
    public static function GetConnection() {
        // Check if we already have a connection object
        if (isset(self::$_connection)) {
            return self::$_connection;
        }

        // Check if we have the params required to establish a new connection
        if (isset(self::$_connectionParams)) {
            try {
                self::$_connection = new Predis\Client(self::$_connectionParams);
                return self::$_connection;
            }
            catch (Exception $ex) {
                // TODO: Revisit exception handling when redis connection fails.
                // But its probably ok to just continue here. Things will just slow-path without redis.
                Log::LogWarning("Failed to get redis connection. Exception: " . $ex->getMessage());
            }
        } else {
            Log::LogWarning("No connection params defined");
        }

        return null;
    }
}

?>