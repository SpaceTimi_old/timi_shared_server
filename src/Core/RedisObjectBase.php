<?php

include_once __SHARED_SRC_DIR."Core/DBObjectBase.php";
include_once __SHARED_SRC_DIR."Core/RedisConnectionManager.php";

abstract class RedisObjectBase extends DBObjectBase {

    protected static $EXPIRE_TIME_DEFAULT_SECONDS = 43200; // Half a day

    public function __construct() {
        parent::__construct(null);
    }

    public function Apply() {
        /** Update the backing db first to avoid inconsistencies */
        if (!parent::Apply()) {
            return false;
        }
        $this->writeToCache();
        return true;
    }

    /**
     * @param $primaryKeyValues array() of primary key values
     * @return string
     */
    private function getRedisKey($primaryKeyValues) {
        $key = Context::GetContext()->appId . '_' .
               get_called_class() . '_' . $this->getRedisKeyPrefix() . '_' .
               implode('_', $primaryKeyValues) . '_';

        /** @var DataMapper $dataMapper */
        $dataMapper = $this->getDataMapper();

        if ($dataMapper->databaseName == __SHARED_DATABASE) {
            $key = $key . SDVManager::GetSharedSDVForTable($this->getDataMapper()->tableName);
        }
        if ($dataMapper->databaseName == __APP_DATABASE) {
            $key = $key . SDVManager::GetAppSDVForTable($this->getDataMapper()->tableName);
        }
        return $key;
    }

    private function writeToCache() {
        /** @var DataMapper $dataMapper */
        $dataMapper = $this->getDataMapper();
        $dataMapper->primaryKeys;
        $primaryKeyValues = array();
        foreach ($dataMapper->primaryKeys as $primaryKey) {
            $primaryKeyValues[] = $this->$primaryKey;
        }

        $connection = RedisConnectionManager::GetConnection();
        $key = $this->getRedisKey($primaryKeyValues);
        $value = json_encode($this);
        $result = $connection->set($key, $value);
        if ($result == 'OK') {
            $result = $connection->expire($key, (int)$this->getExpireTimeSeconds());
            if (empty($result)) {
                Log::LogNotice("Failed to set expire time for key: $key");
            }
        } else {
            Log::LogWarning("Failed to write to redis for key: $key");
        }
    }

    /**
     * @param $primaryKeyValues array() of primary key values
     * @param $loadedObject RedisObjectBase
     */
    protected static function loadByPrimaryKeys($primaryKeyValues, &$loadedObject) {

        /**
         * Check cache
         */
        $connection = RedisConnectionManager::GetConnection();
        $key = $loadedObject->getRedisKey($primaryKeyValues);
        $cachedJson = $connection->get($key);

        if (empty($cachedJson)) {
            /** Not found in cache. Load from DB */
            DBObjectBase::loadByPrimaryKeys($primaryKeyValues, $loadedObject);

            if (!empty($loadedObject)) {
                /** Save to cache for subsequent loads */
                $loadedObject->writeToCache();
            }

        } else {
            /** Initialize from cached value */
            $loadedObject->setFromPropertiesArray(json_decode($cachedJson, true));
        }
    }

    protected function getExpireTimeSeconds() {
        /** Return a random number between 0.8x to 1.2x the default value */
        return (int)(self::$EXPIRE_TIME_DEFAULT_SECONDS * ((float)(rand(800, 1200)) / 1000.0));
    }

    #region abstract
    protected abstract function getRedisKeyPrefix();
    #endregion

}

?>
