<?php

include_once __SHARED_SRC_DIR."Core/CoreUtilities.php";

abstract class DBObjectBase {

    /**
     * Saves the public properties of the object.
     * Used to detect if there are any updates that need to be written to DB (if we are allowed to make updates)
     * @var array
     */
    private $_savedProperties;

    /**
     * @var DataMapper
     */
    private $_dataMapper;

    /**
     * DBObjectBase constructor.
     * If $properties is set, copies object property values from it
     * @param $properties array() from property names => property values
     */
    public function __construct($properties = null) {

        $this->_dataMapper = $this->getDataMapper();

        if (isset($properties)) {
            $this->setFromPropertiesArray($properties);
        }
    }

    /**
     * @param $properties array from property names => property values
     * @return bool
     */
    protected function setFromPropertiesArray($properties) {
        if (isset($properties) && is_array($properties)) {
            $classKeys = $this->_dataMapper->GetClassKeys();
            foreach ($classKeys as $key) {
                $this->$key = $properties[$key];
            }

            /** Only create saved properties if we expect updates in this context */
            if (($this->_dataMapper->canUpdateLambda)()) {
                $this->savePropertyValues();
            }

            return true;
        }
        return false;
    }

    /**
     * @param $primaryKeyValues array() of primary key values
     * @param $loadedObject DBObjectBase out parameter
     */
    protected static function loadByPrimaryKeys($primaryKeyValues, &$loadedObject) {

        $tableName = $loadedObject->_dataMapper->tableName;
        $databaseName = $loadedObject->_dataMapper->databaseName;

        $fieldsClause = implode(', ', $loadedObject->_dataMapper->GetClassKeys());

        $primaryKeys = $loadedObject->_dataMapper->primaryKeys;
        $numKeys = count($primaryKeys);
        $whereClauses = array();
        for ($i = 0; $i < $numKeys; ++$i) {
            $whereClauses[] = $primaryKeys[$i] . " = '" . $primaryKeyValues[$i] . "'";
        }
        $whereClause = " WHERE " . implode(' AND ', $whereClauses);
        $query = "SELECT " . $fieldsClause . " FROM " . $tableName . $whereClause;

        $result = RunQuery($query, $databaseName);

        if (!empty($result)) {
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            if (empty($row)) {
                // No such object in DB
                $loadedObject = null;
                return;
            }

            foreach ($row as $key => $value) {
                $loadedObject->$key = $value;
            }
        }

        /** Only create saved properties if we expect updates in this context */
        if (($loadedObject->_dataMapper->canUpdateLambda)()) {
            $loadedObject->savePropertyValues();
        }
    }

    /**
     * Writes the DBObjectBase to the DB
     * If the element does not exist, it inserts a new one
     * Returns true if write to DB
     * @return bool
     */
    public function Apply() {
        if (!($this->_dataMapper->canUpdateLambda)()) {
            Log::LogWarning("Apply called in context where updates are not allowed for " . $this->_dataMapper->tableName);
            return false;
        }

        if (!$this->hasChanged()) {
            return false;
        }
        if ($this->insertOnDuplicateUpdate() == false) {
            return false;
        }

        if (($this->_dataMapper->canUpdateLambda)()) {
            $this->savePropertyValues();
        }

        return true;
    }

    /**
     * Saves the state of this object's public properties internally
     * so that we can detect if they have been updated
     */
    private function savePropertyValues() {
        $this->_savedProperties = array();
        $keys = $this->_dataMapper->GetClassKeys();
        foreach ($keys as $key) {
            $this->_savedProperties[$key] = $this->$key;
        }
    }

    /**
     * Returns true if any of the public properties have been updated since creation
     * @return bool
     */
    private function hasChanged() {
        if (empty($this->_savedProperties)) {
            return true;
        }
        foreach ($this->_savedProperties as $key => $value) {
            if ($this->$key != $value) {
                return true;
            }
        }
        return false;
    }

    /**
     * Insert a new row in the DB or update existing row. Return true on success, false on failure
     * @return bool
     */
    private function insertOnDuplicateUpdate() {
        $keys = $this->_dataMapper->GetClassKeys();
        $newValues = array();
        $updateKeyValues = array();
        foreach ($keys as $key) {
            $value = $this->$key;
            if ($value == null) {
                $newValues[] = 'null';
                $updateKeyValues[] = $key . ' = null';
            } else {
                $newValues[] = '\'' . $value . '\'';
                $updateKeyValues[] = $key . ' = \'' . $value . '\'';

            }
        }
        $fieldList = ' (' . implode(', ', $keys) . ') ';
        $valueList = ' (' . implode(', ', $newValues) . ') ';
        $updateKeyValueList = implode(', ', $updateKeyValues);

        $query = "INSERT INTO " . $this->_dataMapper->tableName . $fieldList . 'VALUES' . $valueList .
            'ON DUPLICATE KEY UPDATE ' . $updateKeyValueList;

        $autoIncrementField = $this->_dataMapper->autoIncrementField;
        $autoIncrementValue = null;
        if (!RunQuery($query, $this->_dataMapper->databaseName, $autoIncrementValue)) {
            return false;
        }

        if (!empty($autoIncrementField) && empty($this->$autoIncrementField)) {
            if (!empty($autoIncrementValue)) {
                $this->$autoIncrementField = $autoIncrementValue;
            } else {
                Log::LogNotice("Error getting auto increment value for field: $autoIncrementField. Query: $query");
                return false;
            }
        }

        return true;
    }

    /**
     * @return DataMapper
     */
    protected abstract function getDataMapper();
}

?>
