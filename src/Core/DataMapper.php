<?php

class DataMapper {
    /** @var string */
    public $databaseName;
    /** @var string */
    public $tableName;
    /** @var bool */
    public $isUserTable;
    /** @var string */
    public $mappingClassName;
    /** @var string[] */
    public $primaryKeys;
    /** @var callable */
    public $canUpdateLambda;
    /** @var string */
    public $autoIncrementField;

    /**
     * DataMapper constructor.
     * @param $databaseName string
     * @param $tableName string
     * @param $isUserTable bool
     * @param $mappingClassName string
     * @param $primaryKeys string[]
     * @param $canUpdateLambda callable
     */
    public function __construct($databaseName, $tableName, $isUserTable, $mappingClassName, $primaryKeys, $canUpdateLambda, $autoIncrementField = null) {
        $this->databaseName = $databaseName;
        $this->tableName = $tableName;
        $this->isUserTable = $isUserTable;
        $this->mappingClassName = $mappingClassName;
        $this->primaryKeys = $primaryKeys;
        $this->canUpdateLambda = $canUpdateLambda;
        $this->autoIncrementField = $autoIncrementField;
    }

    /**
     * @return string[]
     */
    public function GetClassKeys() {
        return array_keys(get_class_vars($this->mappingClassName));
    }


    #region Static
    /**
     * @var array() of string => DataMapper
     */
    private static $_dataMappers;

    /**
     * @param $name string
     * @param $dataMapper DataMapper
     */
    public static function AddDataMapper($key, $dataMapper) {
        self::$_dataMappers[$key] = $dataMapper;
    }

    /**
     * @param $key string
     * @return DataMapper|null
     */
    public static function GetDataMapper($key) {
        return (array_key_exists($key, self::$_dataMappers) ? self::$_dataMappers[$key] : null);
    }
    #endregion
}

?>