<?php

include_once __SHARED_SRC_DIR."Core/DBObjectListBase.php";

abstract class RedisObjectListBase extends DBObjectListBase {

    protected static $EXPIRE_TIME_DEFAULT_SECONDS = 43200; // Half a day

    /**
     * Checks cache
     * If not found, loads from DB and writes to cache
     * RedisObjectListBase constructor.
     */
    protected function __construct() {
        if (!$this->checkLoadFromCache()) {
            $this->loadFromDB();
            $this->writeToCache();
        }
    }

    /**
     * Writes current state to cache if any list element has updates
     * Returns true if cache was updated
     * @return bool
     */
    public function Apply() {
        /** Update the backing db first to avoid inconsistencies */
        if (!parent::Apply()) {
            return false;
        }
        $this->writeToCache();
        return true;
    }

    /**
     * Returns true if successfully loaded from cache, false otherwise
     * @return bool
     */
    private function checkLoadFromCache() {
        $connection = RedisConnectionManager::GetConnection();
        $key = $this->getRedisKey();
        $json = $connection->get($key);

        if (empty($json)) {
            /** Not found in cache */
            return false;
        }
        return $this->SetFromJsonObjectRepresentation($json);
    }

    // TODO: Implement set dirty and write back to cache
    protected function writeToCache() {
        $connection = RedisConnectionManager::GetConnection();
        $key = $this->getRedisKey();
        if (($value = $this->GetJsonObjectRepresentation()) == false) {
            Log::LogWarning("Error getting json representation for redis object with key: $key");
            return;
        }

        $result = $connection->set($key, $value);
        if ($result == 'OK') {
            $result = $connection->expire($key, (int)$this->getExpireTimeSeconds());
            if (empty($result)) {
                Log::LogNotice("Failed to set expire time for key: $key");
            }
        } else {
            Log::LogWarning("Failed to write to redis for key: $key");
        }
    }

    protected function getRedisKey() {
        $key = Context::GetContext()->appId . '_' .
               get_called_class() . '_' . $this->getRedisKeyPrefix() . '_';

        $dataMapper = $this->getDataMapper();
        if ($dataMapper->databaseName == __SHARED_DATABASE) {
            $key = $key . SDVManager::GetSharedSDVForTable($this->getDataMapper()->tableName);
        }
        if ($dataMapper->databaseName == __APP_DATABASE) {
            $key = $key . SDVManager::GetAppSDVForTable($this->getDataMapper()->tableName);
        }
        return $key;
    }

    protected function getExpireTimeSeconds() {
        /** Return a random number between 0.8x to 1.2x the default value */
        return (int)(self::$EXPIRE_TIME_DEFAULT_SECONDS * ((float)(rand(800, 1200)) / 1000.0));
    }

    #region abstract
    protected abstract function getRedisKeyPrefix();
    #endregion
}