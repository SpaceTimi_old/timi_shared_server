<?php

class Context {
    #region enums
    const CONTEXT_TYPE_API = "CONTEXT_TYPE_API";
    const CONTEXT_TYPE_TOOL = "CONTEXT_TYPE_TOOL";

    const CURRENT_ENVIRONMENT_DEV = "DEV";
    const CURRENT_ENVIRONMENT_STAGING = "STAGING";
    const CURRENT_ENVIRONMENT_PROD = "PROD";

    const API_REQUEST_TYPE_LOGIN = "LOGIN";
    const API_REQUEST_TYPE_CHANGE_EVENT = "CHANGE_EVENT";
    #endregion

    /** @var string */
    public $appName;

    /** @var int */
    public $appId;

    /** @var string enum */
    public $currentEnvironment;

    /** @var string enum */
    public $contextType;

    /** @var string enum */
    public $apiRequestType;

    /** @var User */
    public $user;

    /** @var Account */
    public $account;

    /** @var mixed */
    public $requestMessage;

    /** @var array */
    private $_userSdvDataList;

    /**
     * Private constructor. Use static instance
     */
    private function __construct() { }

    /**
     * @var Context
     */
    private static $_instance;

    public static function GetContext() {
        if (!isset(self::$_instance)) {
            self::$_instance = new Context();
        }
        return self::$_instance;
    }

    /**
     * @param UserSDVListDataModel $userSdvListDataModel
     */
    public function SetUserSdvData($userSdvListDataModel) {
        foreach ($userSdvListDataModel->userSdvDatas as $userSdvData) {
            $this->_userSdvDataList[$userSdvData->tableName] = $userSdvData->versionNumber;
        }
    }

    /**
     * @param string $tableName
     * @return int
     */
    public function GetUserSDVForTableName($tableName) {
        return idx($this->_userSdvDataList, $tableName, -1);
    }
}
?>