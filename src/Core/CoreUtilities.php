<?php
include_once __SHARED_SRC_DIR . "Core/DBConnectionManager.php";

/**
 * @param $query string
 * @param $databaseName string
 * @param $autoIncrementValue out parameter for returning an auto increment field value
 * @return bool|mysqli_result|null
 */
function RunQuery($query, $databaseName, &$autoIncrementValue = null) {
    $connection = DBConnectionManager::OpenOrGetConnection($databaseName);
    if ($connection == null) {
        Log::LogWarning("Problem getting connection. Not running query: $query");
        return null;
    }
    $result = mysqli_query($connection, $query);
    if (!$result) {
        Log::LogWarning("Error running query: $query");
        return null;
    }
    $autoIncrementValue = $connection->insert_id;

    return $result;
}

/**
 * Safely get value from associative array
 * @param $array
 * @param $key
 * @param null $default
 * @return mixed|null
 */
function idx($array, $key, $default = null) {
    if (is_array($array)) {
        // Short circuit to check isset first because its faster.
        // But it has false negatives, so slow path to array_key_exists if it fails
        if (isset($array[$key]) || array_key_exists($key, $array)) {
            return $array[$key];
        }
    }
    return $default;
}

function idxidx($array, $key1, $key2, $default = null) {
    $value1 = idx($array, $key1, null);
    if (!empty($value1)) {
        return idx($value1, $key2, $default);
    }
    return $default;
}

function getDateTimeNow() {
    return date('Y-m-d H:i:s');
}
?>
