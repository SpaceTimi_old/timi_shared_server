<?php

class DBConnectionManager {

    /**
     * @var DBConnectionParams[] indexed by database name
     */
    private static $_connectionParams = array();

    /**
     * @var mysqli[] indexed by database name
     */
    private static $_connections = array();

    /**
     * Private constructor. Use static methods
     */
    private function __construct() { }

    /**
     * Clean up
     */
    public function __destruct() {
        foreach (self::$_connections as $connection) {
            mysqli_close($connection);
        }
    }

    public static function SetParamsForConnection($dbServer, $dbUsername, $dbPassword, $databaseName) {
        self::$_connectionParams[$databaseName] = new DBConnectionParams($dbServer, $dbUsername, $dbPassword, $databaseName);
    }

    /**
     * @param $databaseName string
     * @return mysqli|null
     */
    public static function OpenOrGetConnection($databaseName) {
        // Check if we already have a connection open to this database
        if (isset(self::$_connections[$databaseName])) {
            return self::$_connections[$databaseName];
        }

        // Check if we have the params required to establish a new connection
        $connectionParams = null;
        if (!isset(self::$_connectionParams[$databaseName])) {
            Log::LogWarning("No connection params defined for database: " . $databaseName);
            return null;
        }
        $connectionParams = self::$_connectionParams[$databaseName];

        // Connect to MySQL and select the database
        $connection = mysqli_connect($connectionParams->dbServer, $connectionParams->dbUsername, $connectionParams->dbPassword);
        if (mysqli_connect_errno()) {
            Log::LogWarning("Failed to connect to MySQL: " . mysqli_connect_error());
            return null;
        }
        mysqli_select_db($connection, $databaseName);

        // Cache the connection
        self::$_connections[$databaseName] = $connection;

        return $connection;
    }

    /**
     * @param $connection mysqli
     */
    public static function CloseConnection($connection) {
        $key = array_search($connection, self::$_connections);
        $key != false or Log::LogNotice("No such connection");
        unset(self::$_connections[$key]);
    }
}

class DBConnectionParams {
    public $dbServer;
    public $dbUsername;
    public $dbPassword;
    public $databaseName;

    /**
     * DBConnectionParams constructor.
     * @param $dbServer string
     * @param $dbUsername string
     * @param $dbPassword string
     * @param $databaseName string
     */
    public function __construct($dbServer, $dbUsername, $dbPassword, $databaseName) {
        if (empty($dbServer) || empty($dbUsername) || empty($dbPassword) || empty($databaseName)) {
            Log::LogWarning("DB connection parameters not valid");
            return;
        }
        $this->dbServer = $dbServer;
        $this->dbUsername = $dbUsername;
        $this->dbPassword = $dbPassword;
        $this->databaseName = $databaseName;
    }


}


?>