<?php

abstract class DBObjectListBase {

    /**
     * @var DBObjectBase[]
     */
    protected $_list;

    protected function __construct() {
        $this->loadFromDB();
    }

    /**
     * @return DBObjectBase[]
     */
    protected function getList() {
        return $this->_list;
    }

    /**
     * Applies changes to the DB if any list element has updates
     * Returns true if DB was updated
     * @return bool
     */
    public function Apply() {
        if (!($this->getDataMapper()->canUpdateLambda)()) {
            Log::LogWarning("Apply called in context where updates are not allowed for " . $this->getDataMapper()->tableName);
            return false;
        }

        $didUpdate = false;
        foreach ($this->_list as $listElement) {
            $didUpdate |= $listElement->Apply();
        }

        return $didUpdate;
    }

    /**
     * @return string
     */
    public function GetJsonObjectRepresentation() {
        if (($json = json_encode($this->_list)) == false) {
            Log::LogWarning("Error while json encoding: " . json_last_error_msg() .
                ", object:" . substr(print_r($this->_list, true ), 0, 400));
            echo json_last_error_msg();
        }
        return $json;
    }

    /**
     * @param $json
     * @return bool success
     */
    public function SetFromJsonObjectRepresentation($json) {
        $this->_list = array();
        $mappingClassType = $this->getDataMapper()->mappingClassName;

        if (empty($json)) {
            Log::LogWarning("Trying to deserialize empty json");
            return false;
        }
        $deserializedList = json_decode($json, true);

        foreach ($deserializedList as $deserializedListElement) {
            $listElement = new $mappingClassType($deserializedListElement);
            $this->_list[] = $listElement;
        }

        return true;
    }

    /**
     * Loads the internal _list from the DB
     */
    protected function loadFromDB() {
        /** @var DataMapper $dataMapper */
        $dataMapper = $this->getDataMapper();
        $mappingClassType = $dataMapper->mappingClassName;

        // TODO: Get rid of this. Use it from the list element object instead
        $classKeys = $dataMapper->GetClassKeys();
        $fieldsClause = implode(', ', $classKeys);

        /* TODO: Add WHERE filter clause */
        $query = "SELECT " . $fieldsClause . " FROM " . $dataMapper->tableName;

        $connection = DBConnectionManager::OpenOrGetConnection($dataMapper->databaseName);
        $result = mysqli_query($connection, $query);

        if (empty($result)) {
            Log::LogWarning("Error fetching from database. Query: " . $query);
            return;
        }

        $this->_list = array();
        foreach ($result as $row) {
            $listElement = new $mappingClassType($row);
            $this->_list[] = $listElement;
        }
    }

    /**
     * @return DataMapper
     */
    protected abstract function getDataMapper();
}

