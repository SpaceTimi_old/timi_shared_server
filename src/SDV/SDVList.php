<?php

include_once __SHARED_SRC_DIR . "Core/RedisObjectListBase.php";
include_once __SHARED_SRC_DIR . "SDV/SDV.php";

class SDVList extends RedisObjectListBase {

    /** @var SDVList */
    private static $_instance;

    /**
     * @return SDVList
     */
    public static function GetInstance() {
        if (!isset(self::$_instance)) {
            self::$_instance = new SDVList();
        }
        return self::$_instance;
    }

    public function GetSDVByAppIdTableName($appId, $tableName) {
        $list = $this->getList();
        /** @var SDV $sdv */
        foreach ($list as $sdv) {
            if ($sdv->app_id == $appId && $sdv->table_name == $tableName) {
                return $sdv;
            }
        }
        return null;
    }

    /**
     * @param int $appId
     * @return SDV[]
     */
    public function GetAllSDVsForAppId($appId) {
        $result = array();

        $list = $this->getList();
        /** @var SDV $sdv */
        foreach ($list as $sdv) {
            if ($sdv->app_id == $appId) {
                $result[] = $sdv;
            }
        }
        return $result;
    }

    /**
     * @return DataMapper
     */
    protected function getDataMapper() {
        return DataMapper::GetDataMapper(SDV::GetClassName());
    }

    protected function getRedisKey() {
        /***
         * Note: We override the getRedisKey() function in SDVList because we don't want this to
         * call into SDVManager and cause an infinite stack.
         * Fix this by having SDVList not derive from RedisObjectListBase?
         */
        return get_called_class() . '_' . $this->getRedisKeyPrefix();
    }

    protected function getRedisKeyPrefix() {
        return 'v0';
    }
}
