<?php

class UserSDVData {

    /** @var string */
    public $tableName;

    /** @var int */
    public $versionNumber;

    public function __construct($tableName, $versionNumber) {
        $this->tableName = $tableName;
        $this->versionNumber = $versionNumber;
    }

}
