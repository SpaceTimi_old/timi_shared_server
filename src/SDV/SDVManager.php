<?php

include_once __SHARED_SRC_DIR . "SDV/SDVList.php";
include_once __SHARED_SRC_DIR . "SDV/FallbackSDVs.php";

class SDVManager {

    /**
     * @return int
     */
    public static function GetAppSDVForTable($tableName) {
        $appId = Context::GetContext()->appId;
        $sdv = SDVList::GetInstance()->GetSDVByAppIdTableName($appId, $tableName);
        if (!empty($sdv)) {
            return $sdv->version_number;
        }
        return GetFallbackSDVForAppId($appId);
    }

    /**
     * @return int
     */
    public static function UpdateAppSDVForTable($tableName) {
        $appId = Context::GetContext()->appId;
        $sdv = SDVList::GetInstance()->GetSDVByAppIdTableName($appId, $tableName);
        if (empty($sdv)) {
            Log::LogAndTriggerFatalError("No sdv exists for table name: $tableName in app " . Context::GetContext()->appName);
        }

        ++$sdv->version_number;

        SDVList::GetInstance()->Apply();
        return $sdv->version_number;
    }

    /**
     * @return int
     */
    public static function GetSharedSDVForTable($tableName) {
        $sdv = SDVList::GetInstance()->GetSDVByAppIdTableName(AppIds::$SHARED_APP_ID, $tableName);
        if (!empty($sdv)) {
            return $sdv->version_number;
        }
        return GetFallbackSDVForAppId(AppIds::$SHARED_APP_ID);
    }

    /**
     * @return int
     */
    public static function UpdateSharedSDVForTable($tableName) {
        $sdv = SDVList::GetInstance()->GetSDVByAppIdTableName(AppIds::$SHARED_APP_ID, $tableName);
        if (empty($sdv)) {
            Log::LogAndTriggerFatalError("No sdv exists for table name: $tableName in app " . Context::GetContext()->appName);
        }

        ++$sdv->version_number;

        SDVList::GetInstance()->Apply();
        return $sdv->version_number;
    }

    /**
     * @return array of string => int
     */
    public static function GetAllAppSDVs() {
        $result = array();

        $appId = Context::GetContext()->appId;
        $sdvs = SDVList::GetInstance()->GetAllSDVsForAppId($appId);
        foreach ($sdvs as $sdv) {
            $result[$sdv->table_name] = $sdv->version_number;
        }
        return $result;
    }

    /**
     * @return array of string => int
     */
    public static function GetAllSharedSDVs() {
        $result = array();

        $sdvs = SDVList::GetInstance()->GetAllSDVsForAppId(AppIds::$SHARED_APP_ID);
        foreach ($sdvs as $sdv) {
            $result[$sdv->table_name] = $sdv->version_number;
        }
        return $result;
    }
}



?>
