<?php

include_once __SHARED_SRC_DIR . "Core/DBObjectBase.php";
include_once __SHARED_SRC_DIR . "Core/DataMapper.php";

class SDV extends DBObjectBase {

    public $app_id;
    public $table_name;
    public $version_number;

    public static function GetClassName() {
        return get_class();
    }

    protected function getDataMapper() {
        return DataMapper::GetDataMapper(self::GetClassName());
    }
}

DataMapper::AddDataMapper(SDV::GetClassName(),
    new DataMapper(__SHARED_DATABASE,
        'meta_sdv',
        false,
        SDV::GetClassName(),
        array('app_id', 'table_name'),
        function () {
            return (Context::GetContext()->contextType == Context::CONTEXT_TYPE_TOOL);
        }));
