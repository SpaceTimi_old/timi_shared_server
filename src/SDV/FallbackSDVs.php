<?php

/**
 * @param int $appId
 * @return int
 */
function GetFallbackSDVForAppId($appId) {
    $fallbackSDVs = array(
        1 => 1,         // shared
        2 => 1,         // bonda
        3 => 1          // std
    );
    return idx($fallbackSDVs, $appId, 0);
}
