<?php

include_once __SHARED_SRC_DIR . "SDV/UserSDVData.php";

class UserSDVListDataModel extends DataModelBase {

    /** @var UserSDVData[] */
    public $userSdvDatas;

    #region IDataModelDescriptor
    /**
     * @return bool
     */
    public function IsUserDataModel() {
        return true;
    }

    /**
     * @return string[]
     */
    public function GetDependencyTableNames() {
        return array(DataMapper::GetDataMapper(SDV::GetClassName())->tableName);
    }

    /**
     * @return int
     */
    public function GetAppId() {
        return Context::GetContext()->appId;
    }
    #endregion
}
