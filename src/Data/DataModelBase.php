<?php

include_once __SHARED_SRC_DIR."Data/IDataModelDescriptor.php";

abstract class DataModelBase implements IDataModelDescriptor {

    /**
     * @return string
     */
    public function GetTypeName() {
        return get_class($this);
    }

    /**
     * @return string
     */
    public function GetJsonRepresentation() {
        return json_encode($this);
    }

    /**
     * @return IDataModelDescriptor
     */
    public function GetDataModelDescriptor() {
        return $this;
    }
}

?>
