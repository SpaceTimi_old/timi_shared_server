<?php

include_once __SHARED_SRC_DIR . "Data/DataModelJsonData.php";

class ServerResponseMessage {

    /** @var int */
    public $accountId;

    /** @var int */
    public $userId;

    /** @var DataModelJsonData[] */
    public $dataModelJsonDatas;

    /**
     * @param int $accountId
     * @param int $userId
     * @param DataModelBase[] $dataModels
     */
    public function prepareMessage($accountId, $userId, $dataModels) {
        $this->accountId = $accountId;
        $this->userId = $userId;

        $dataModelsToSendToClient = $this->SelectDataModelsToSendToClient($dataModels);

        $this->dataModelJsonDatas = array();
        /** @var DataModelBase $dataModel */
        foreach ($dataModelsToSendToClient as $dataModel) {
            $this->dataModelJsonDatas[] = new DataModelJsonData($dataModel->GetTypeName(),
                                                                $dataModel->GetJsonRepresentation());
        }
    }


    /**
     * @param DataModelBase[] $dataModels
     * @return  DataModelBase[]
     */
    private function SelectDataModelsToSendToClient($dataModels) {
        $dataModelsToSendToClient = array();
        foreach ($dataModels as $dataModel) {
            $dataModelDescriptor = $dataModel->GetDataModelDescriptor();
            if ($this->ShouldSendDataModelToClient($dataModelDescriptor)) {
                $dataModelsToSendToClient[] = $dataModel;
            }
        }

        return $dataModelsToSendToClient;
    }

    /**
     * @param IDataModelDescriptor $dataModelDescriptor
     * @return bool
     */
    private function ShouldSendDataModelToClient($dataModelDescriptor) {
        if ($dataModelDescriptor->IsUserDataModel()) {
            // Always send down user data models
            return true;
        }
        $dependencyTableNames = $dataModelDescriptor->GetDependencyTableNames();
        foreach ($dependencyTableNames as $dependencyTableName) {
            $userSdvVersionNumber = Context::GetContext()->GetUserSDVForTableName($dependencyTableName);
            $currentSdvVersionNumber = 0;
            if ($dataModelDescriptor->GetAppId() == AppIds::$SHARED_APP_ID) {
                $currentSdvVersionNumber = SDVManager::GetSharedSDVForTable($dependencyTableName);
            } else {
                $currentSdvVersionNumber = SDVManager::GetAppSDVForTable($dependencyTableName);
            }
            if ($userSdvVersionNumber < $currentSdvVersionNumber) {
                return true;
            }
        }

        return false;
    }

}



?>
