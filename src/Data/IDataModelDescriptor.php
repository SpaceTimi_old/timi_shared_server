<?php

interface IDataModelDescriptor {
    /**
     * @return bool
     */
    public function IsUserDataModel();

    /**
     * @return string[]
     */
    public function GetDependencyTableNames();

    /**
     * @return int
     */
    public function GetAppId();
}
